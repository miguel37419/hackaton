package com.hackaton.servicios;

import com.hackaton.modelos.Cliente;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioCliente {

    public Page<Cliente> obtenerClientes(int pagina, int cantidad);
    public List<Cliente> obtenerClientesTotales();
    public void insertarClienteNuevo(Cliente cliente);
    public Cliente obtenerCliente(String documento);
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);
    public void borrarCliente(String documento);
    public void agregarReclamoCliente(String documento, String numeroReclamo);
    public List<String> obtenerReclamosCliente(String documento);


}
