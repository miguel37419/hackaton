package com.hackaton.servicios;

import com.hackaton.modelos.Producto;
import com.hackaton.modelos.Reclamo;

import java.util.List;

public interface ServicioProducto {

    public List<Producto> obtenerProductos();
    public void insertarProductoNuevo(Producto producto);
    public Producto obtenerProducto(String codigo);
    public void guardarProducto(Producto producto);
    public void emparcharProducto(Producto parche);
    public  void borrarProducto(String codigo);

}
