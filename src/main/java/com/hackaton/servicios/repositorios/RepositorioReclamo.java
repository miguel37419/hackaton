package com.hackaton.servicios.repositorios;

import com.hackaton.modelos.Reclamo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioReclamo extends MongoRepository<Reclamo,String> {
}
