package com.hackaton.servicios.repositorios;

import com.hackaton.modelos.Cliente;
import com.hackaton.modelos.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioProducto extends MongoRepository<Producto,String> {
}
