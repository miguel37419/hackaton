package com.hackaton.servicios.repositorios;

import com.hackaton.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCliente extends MongoRepository<Cliente,String> {
}
