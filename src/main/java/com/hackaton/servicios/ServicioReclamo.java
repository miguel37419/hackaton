package com.hackaton.servicios;

import com.hackaton.modelos.Reclamo;

import java.util.List;

public interface ServicioReclamo {

    public List<Reclamo> obtenerReclamos();

    public void insertarReclamoNuevo(Reclamo reclamo, String nroProducto);

    public Reclamo obtenerReclamo(String numero);
    public void guardarReclamo(Reclamo cuenta);
    public void emparcharReclamo(Reclamo parche);

    public  void borrarReclamo(String numero);

    public Reclamo obtenerReclamoCliente(String documento, String nroReclamo);

    public void eliminarReclamoCliente(String documento, String nroReclamo);

    public void reemplazarReclamoCliente(String documento, Reclamo reclamo);

    public void emparcharReclamoCliente(String documento, Reclamo reclamo);
}
