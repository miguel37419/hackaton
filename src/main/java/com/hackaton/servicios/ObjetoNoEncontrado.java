package com.hackaton.servicios;

public class ObjetoNoEncontrado extends RuntimeException{
    public ObjetoNoEncontrado(String mensaje) {super(mensaje);}
}
