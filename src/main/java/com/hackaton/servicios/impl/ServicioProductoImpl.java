package com.hackaton.servicios.impl;

import com.hackaton.modelos.Producto;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioProducto;
import com.hackaton.servicios.repositorios.RepositorioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;


    @Override
    public List<Producto> obtenerProductos() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public void insertarProductoNuevo(Producto producto) {

        this.repositorioProducto.insert(producto);
    }

    @Override
    public Producto obtenerProducto(String codigo) {
        final Optional<Producto> productoQuizas = this.repositorioProducto.findById(codigo);
        if(!productoQuizas.isPresent())
            throw new ObjetoNoEncontrado("El producto con el codigo: "+codigo+ " no existe");

        return  productoQuizas.get();
    }

    @Override
    public void guardarProducto(Producto producto) {
        if(!this.repositorioProducto.existsById(producto.codigo))
            throw new ObjetoNoEncontrado("El producto con el codigo: "+producto.codigo+ " no existe");
        this.repositorioProducto.save(producto);
    }

    @Override
    public void emparcharProducto(Producto parche) {
        final Producto existente = obtenerProducto(parche.codigo);
        if(!this.repositorioProducto.existsById(parche.codigo))
            throw new ObjetoNoEncontrado("El producto con el codigo: "+parche.codigo+ " no existe");

        if(parche.descripcion != null)
            existente.descripcion = parche.descripcion;

        this.repositorioProducto.save(existente);
    }

    @Override
    public void borrarProducto(String codigo) {
        if(!this.repositorioProducto.existsById(codigo))
            throw new ObjetoNoEncontrado("El producto con el codigo: "+codigo+ " no existe");
        this.repositorioProducto.deleteById(codigo);
    }
}
