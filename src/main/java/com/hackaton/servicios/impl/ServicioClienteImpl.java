package com.hackaton.servicios.impl;

import com.hackaton.modelos.Cliente;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioCliente;
import com.hackaton.servicios.ServicioReclamo;
import com.hackaton.servicios.repositorios.RepositorioCliente;
import com.hackaton.servicios.repositorios.RepositorioReclamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioReclamo repositorioReclamo;

    @Override
    public Page<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));    }

    @Override
    public List<Cliente> obtenerClientesTotales() {
        return this.repositorioCliente.findAll();
    }


    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final  Optional<Cliente> clienteQuizas = this.repositorioCliente.findById(documento);
        if(!clienteQuizas.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: " +documento );
        return  clienteQuizas.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if(!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: "+ cliente.documento);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = obtenerCliente(parche.documento);

        if(!this.repositorioCliente.existsById(parche.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: "+ parche.documento);
        if(parche.edad != null)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;


        this.repositorioCliente.save(existente);
    }

    @Override
    public void borrarCliente(String documento) {
        if (!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No se puede eliminar el cliente con documento: "+ documento+ " ya que no existe");
        this.repositorioCliente.deleteById(documento);
    }

    @Override
    public List<String> obtenerReclamosCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return  cliente.codigosReclamos;
    }

    @Override
    public void agregarReclamoCliente(String documento, String numeroReclamo) {
        final Cliente cliente = obtenerCliente(documento);

        if(!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No existe cliente con el documento: "+ documento);

        cliente.codigosReclamos.add(numeroReclamo);

        this.repositorioCliente.save(cliente);
    }
}
