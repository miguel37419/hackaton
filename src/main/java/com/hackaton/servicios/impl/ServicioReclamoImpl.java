package com.hackaton.servicios.impl;
import com.hackaton.comun.RestTmeplateInvoker;
import com.hackaton.modelos.Cliente;
import com.hackaton.modelos.Message;
import com.hackaton.modelos.Producto;
import com.hackaton.modelos.Reclamo;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioCliente;
import com.hackaton.servicios.ServicioProducto;
import com.hackaton.servicios.ServicioReclamo;
import com.hackaton.servicios.repositorios.RepositorioReclamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ServicioReclamoImpl implements ServicioReclamo {

    @Autowired
    RepositorioReclamo repositorioReclamo;

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    RestTmeplateInvoker restTmeplateInvoker;

    @Override
    public List<Reclamo> obtenerReclamos() {
        return this.repositorioReclamo.findAll();
    }


    @Override
    public void insertarReclamoNuevo(Reclamo reclamo, String nroProducto) {

        final Cliente cliente = this.servicioCliente.obtenerCliente(reclamo.dniCliente);
        final Producto producto = this.servicioProducto.obtenerProducto(nroProducto);


        if(reclamo.correoCliente == null)
            reclamo.correoCliente = cliente.correo;
        if(reclamo.telefonoCliente == null)
            reclamo.telefonoCliente = cliente.telefono;
        if(reclamo.direccionCliente  == null)
            reclamo.direccionCliente  = cliente.direccion;
        if(reclamo.producto  == null)
            reclamo.producto  = producto.descripcion;
        if(reclamo.descripcion  != null){
            Message mensaje = restTmeplateInvoker.ofuscateText(reclamo.descripcion);
            reclamo.descripcion  = mensaje.description;
        }
        this.servicioCliente.agregarReclamoCliente(reclamo.dniCliente, reclamo.nroReclamo);
        this.repositorioReclamo.insert(reclamo);
    }

    @Override
    public Reclamo obtenerReclamo(String numero) {
        final Optional<Reclamo> reclamoQuizas = this.repositorioReclamo.findById(numero);
        if(!reclamoQuizas.isPresent())
            throw new ObjetoNoEncontrado("El reclamo con el numero: "+numero+ " no existe");
        final Reclamo reclamoEncontrado= reclamoQuizas.get();
        if (reclamoEncontrado.dniCliente == null )
            throw new ObjetoNoEncontrado("El reclamo  no tiene cliente ingresado");

        return  reclamoQuizas.get();
    }

    @Override
    public void guardarReclamo(Reclamo reclamo) {
        if(!this.repositorioReclamo.existsById(reclamo.nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo con el numero: "+reclamo.nroReclamo+ " no existe");
        if(reclamo.descripcion  != null){
            Message mensaje = restTmeplateInvoker.ofuscateText(reclamo.descripcion);
            reclamo.descripcion  = mensaje.description;
        }
        this.repositorioReclamo.save(reclamo);
    }

    @Override
    public void emparcharReclamo(Reclamo parche) {
        final Reclamo existente = obtenerReclamo(parche.nroReclamo);
        if(!this.repositorioReclamo.existsById(parche.nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo con el numero: "+parche.nroReclamo+ " no existe");

        if(parche.descripcion != null){
            Message mensaje = restTmeplateInvoker.ofuscateText(parche.descripcion);
            existente.descripcion = mensaje.description;
        }


        if(parche.estado != null)
            existente.estado = parche.estado;

        if(parche.dniCliente != null)
            existente.dniCliente = parche.dniCliente;

        this.repositorioReclamo.save(existente);
    }

    @Override
    public void borrarReclamo(String numero) {
        if(!this.repositorioReclamo.existsById(numero))
            throw new ObjetoNoEncontrado("El reclamo con el numero: "+numero+ " no existe");
        this.repositorioReclamo.deleteById(numero);
    }
    @Override
    public Reclamo obtenerReclamoCliente(String documento, String nroReclamo) {
        if(!this.repositorioReclamo.existsById(nroReclamo))
            throw new ObjetoNoEncontrado("No existe el reclamo con numero: "+ nroReclamo);
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);

        if(!cliente.codigosReclamos.contains(nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo no esta asociado al cliente");
        final var reclamo= this.repositorioReclamo.findById(nroReclamo);

        if (!reclamo.isPresent())
            throw new ObjetoNoEncontrado("El reclamo con numero: "+nroReclamo+ " no existe");

        return reclamo.get();
    }

    @Override
    public void eliminarReclamoCliente(String documento, String nroReclamo) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Reclamo> cuentaQuizas = this.repositorioReclamo.findById(nroReclamo);
        if (!cuentaQuizas.isPresent())
            throw new ObjetoNoEncontrado("El reclamo con numero: "+ nroReclamo + " no existe");

        if(!cliente.codigosReclamos.contains(nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo no esta asociado al cliente");

        final var reclamo= cuentaQuizas.get();
        reclamo.estado = "ATENDIDO";
        cliente.codigosReclamos.remove(nroReclamo);
        this.servicioCliente.guardarCliente(cliente);
        guardarReclamo(reclamo);

    }

    @Override
    public void reemplazarReclamoCliente(String documento, Reclamo reclamo) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Reclamo> reclamoQuizas = this.repositorioReclamo.findById(reclamo.nroReclamo);
        if (!reclamoQuizas.isPresent())
            throw new ObjetoNoEncontrado("El reclamo con numero: "+reclamo.nroReclamo+ " no existe");
        if (!cliente.codigosReclamos.contains(reclamo.nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo  no esta asociado al cliente");
        guardarReclamo(reclamo);
    }

    @Override
    public void emparcharReclamoCliente(String documento, Reclamo reclamo) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Reclamo> reclamoQuizas = this.repositorioReclamo.findById(reclamo.nroReclamo);
        if (!reclamoQuizas.isPresent())
            throw new ObjetoNoEncontrado("El reclamo con numero: "+reclamo.nroReclamo+ " no existe");
        if (!cliente.codigosReclamos.contains(reclamo.nroReclamo))
            throw new ObjetoNoEncontrado("El reclamo no esta asociada al cliente");
        emparcharReclamo(reclamo);
    }

}
