package com.hackaton.comun;

import com.hackaton.controlador.Rutas;
import com.hackaton.modelos.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTmeplateInvoker {

    @Autowired
    RestTemplate restTemplate;

    public Message ofuscateText(String desc) {
        Message request = new Message();
        request.description = desc;
        String fooResourceUrl =  Rutas.EXTERNAL_BASE_URL  + "/api/v1/ofuscate";
        Message response = restTemplate.postForObject(fooResourceUrl, request, Message.class);
        return response;
    }
}
