package com.hackaton.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OfuscacionInformacionSensibleUtil {

    public String ofuscarInformacionSensible(String textoDeEntrada) {
        String textoOfuscado;

        textoOfuscado = ofuscarInformacionDeCuenta(textoDeEntrada);
        textoOfuscado = ofuscarInformacionDeTarjeta(textoOfuscado);

        return textoOfuscado;
    }

    private String ofuscarInformacionDeCuenta(String textoDeEntrada) {
        StringBuffer textoOfuscado = new StringBuffer(textoDeEntrada);

        List<Coincidencia> listaDeCoincidencias = obtenerCoincidenciasPorInformacionDeCuenta(textoDeEntrada);

        listaDeCoincidencias.forEach(coincidencia -> {
            textoOfuscado.replace(coincidencia.getIndiceInicio(), coincidencia.getIndiceFin(),
                    reemplazar(coincidencia.getFrase(), 9, 6));
        });

        return textoOfuscado.toString();
    }

    private List<Coincidencia> obtenerCoincidenciasPorInformacionDeCuenta(String textoDeEntrada) {
        String expresionRegular = "\\d{18}|\\d{20}|(\\d){4}[- ](\\d){4}[- ](\\d){10}|(\\d){4}[- ](\\d){4}[- ](\\d){10}[- ](\\d){2}";

        return obtenerCoincidencias(textoDeEntrada, expresionRegular);
    }

    private String ofuscarInformacionDeTarjeta(String textoDeEntrada) {
        StringBuffer textoOfuscado = new StringBuffer(textoDeEntrada);

        List<Coincidencia> listaDeCoincidencias = obtenerCoincidenciasPorInformacionDeTarjeta(textoDeEntrada);

        listaDeCoincidencias.forEach(coincidencia -> {
            textoOfuscado.replace(coincidencia.getIndiceInicio(), coincidencia.getIndiceFin(),
                    reemplazar(coincidencia.getFrase(), 5, 8));
        });

        return textoOfuscado.toString();
    }

    private List<Coincidencia> obtenerCoincidenciasPorInformacionDeTarjeta(String textoDeEntrada) {
        String expresionRegular = "\\d{16}|(\\d){4}[- ](\\d){4}[- ](\\d){4}[- ](\\d){4}";

        return obtenerCoincidencias(textoDeEntrada, expresionRegular);
    }

    private List<Coincidencia> obtenerCoincidencias(String textoDeEntrada, String expresionRegular) {
        List<Coincidencia> listaDeCoincidencias = new ArrayList<>();

        Pattern pattern = Pattern.compile(expresionRegular);

        Matcher matcher = pattern.matcher(textoDeEntrada);

        while (matcher.find()) {
            Coincidencia coincidencia = new Coincidencia(matcher.start(), matcher.end(), matcher.group());

            listaDeCoincidencias.add(coincidencia);
        }

        return listaDeCoincidencias;
    }

    public String reemplazar(String textoDeEntrada, int posIni, int cant) {
        StringBuilder textoOfuscado = new StringBuilder();
        Pattern pattern = Pattern.compile("\\d");
        for (int x = 0; x < textoDeEntrada.length(); x++) {
            Matcher matcher = pattern.matcher(String.valueOf(textoDeEntrada.charAt(x)));
            if (x >= posIni - 1 && x < posIni + cant - 1) {
                if (matcher.find()) {
                    textoOfuscado.append("*");
                } else {
                    textoOfuscado.append(textoDeEntrada.charAt(x));
                    cant++;
                }
            } else {
                textoOfuscado.append(textoDeEntrada.charAt(x));
                if (!matcher.find()) {
                    posIni++;
                }
            }
        }
        return textoOfuscado.toString();
    }
}
