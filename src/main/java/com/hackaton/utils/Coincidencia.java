package com.hackaton.utils;

public class Coincidencia {
    int indiceInicio;
    int indiceFin;
    String frase;

    public Coincidencia(int indiceInicio, int indiceFin, String frase) {
        this.indiceInicio = indiceInicio;
        this.indiceFin = indiceFin;
        this.frase = frase;
    }

    public int getIndiceInicio() {
        return indiceInicio;
    }

    public void setIndiceInicio(int indiceInicio) {
        this.indiceInicio = indiceInicio;
    }

    public int getIndiceFin() {
        return indiceFin;
    }

    public void setIndiceFin(int indiceFin) {
        this.indiceFin = indiceFin;
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }
}
