package com.hackaton.controlador;

import com.hackaton.modelos.Reclamo;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioCliente;
import com.hackaton.servicios.ServicioProducto;
import com.hackaton.servicios.ServicioReclamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.RECLAMOS)
public class ControladorReclamo {
    @Autowired
    ServicioReclamo servicioReclamo;
    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;
    @GetMapping
    public List<Reclamo> obtenerReclamos() {
        try {
            return this.servicioReclamo.obtenerReclamos();
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PostMapping("/{nroProducto}")
    public ResponseEntity agregarReclamo(@RequestBody Reclamo reclamo, @PathVariable String nroProducto) {

        try {
            this.servicioReclamo.insertarReclamoNuevo(reclamo, nroProducto);

            final var link
                    = methodOn(ControladorReclamo.class)
                    .obtenerUnReclamo(reclamo.nroReclamo);

            final var enlaceEsteDocumento = linkTo(link).toUri();

            return ResponseEntity
                    .ok()
                    .location(enlaceEsteDocumento)
                    .build();
        } catch(ObjetoNoEncontrado x ){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @GetMapping("/{nroReclamo}")
    public EntityModel<Reclamo> obtenerUnReclamo(@PathVariable String nroReclamo) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnReclamo(nroReclamo)).withSelfRel();
            final Link cliente = linkTo(methodOn(this.getClass()).obtenerUnReclamo(nroReclamo)).withRel(" El cliente es: ");
            final Reclamo reclamo= this.servicioReclamo.obtenerReclamo(nroReclamo);
            return EntityModel.of(reclamo).add(self,cliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PutMapping("/{nroReclamo}")
    public void reemplazarUnReclamo(@PathVariable("nroReclamo") String nroReclamo,
                                    @RequestBody Reclamo reclamo) {
        try {
            reclamo.nroReclamo = nroReclamo;
            this.servicioReclamo.guardarReclamo(reclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PatchMapping("/{nroReclamo}")
    public void emparcharUnReclamo(@PathVariable("nroReclamo") String nroReclamo,
                                   @RequestBody Reclamo reclamo) {
        try {
            reclamo.nroReclamo = nroReclamo;
            this.servicioReclamo.emparcharReclamo(reclamo);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @DeleteMapping("/{nroReclamo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnReclamo(@PathVariable String nroReclamo) {
        try {
            this.servicioReclamo.borrarReclamo(nroReclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, x.getMessage());
        }
    }
}
