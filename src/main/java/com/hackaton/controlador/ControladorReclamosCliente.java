package com.hackaton.controlador;

import com.hackaton.modelos.Reclamo;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioCliente;
import com.hackaton.servicios.ServicioReclamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/reclamos")
public class ControladorReclamosCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioReclamo servicioReclamo;


    public static class DatosEntradaReclamo {
        public String codigoReclamo;
    };

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarReclamoCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaReclamo datosEntradaReclamo) {
        try {
            this.servicioCliente.agregarReclamoCliente(documento, datosEntradaReclamo.codigoReclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<List<String>> obtenerReclamosCliente(@PathVariable String documento) {
        try {
            final var reclamosCliente = this.servicioCliente.obtenerReclamosCliente(documento);
            return ResponseEntity.ok(reclamosCliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> obtenerCuentaCliente(documento,numeroCuenta)
    @GetMapping("/{nroReclamo}")
    public ResponseEntity<Reclamo> obtenerReclamoCliente(@PathVariable String documento, @PathVariable String nroReclamo) {
        try {
            final Reclamo reclamo = this.servicioReclamo.obtenerReclamoCliente(documento, nroReclamo);
            return ResponseEntity.ok(reclamo);

        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }


    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> eliminarCuentaCliente(documento,numeroCuenta)
    @DeleteMapping("/{nroReclamo}")
    public ResponseEntity eliminarReclamoCliente(@PathVariable String documento, @PathVariable String nroReclamo) {
        try {
            this.servicioReclamo.eliminarReclamoCliente(documento, nroReclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public void reemplazarReclamoCliente(@PathVariable("documento") String nroDocumento,
                                        @RequestBody Reclamo reclamo) {
        try {
            this.servicioReclamo.reemplazarReclamoCliente(nroDocumento, reclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PatchMapping
    public void emparcharReclamoCliente(@PathVariable("documento") String nroDocumento,
                                       @RequestBody Reclamo reclamo) {
        try {
            this.servicioReclamo.emparcharReclamoCliente(nroDocumento, reclamo);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

}
