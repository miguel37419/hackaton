package com.hackaton.controlador;

import com.hackaton.modelos.Cliente;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @GetMapping("/paginador")
    public PagedModel<EntityModel<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            final var pageSinMetadatos = this.servicioCliente.obtenerClientes(pagina, cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                                    .withSelfRel().withTitle("Este cliente"),
                            linkTo(methodOn(ControladorReclamosCliente.class).obtenerReclamosCliente(x.documento))
                                    .withRel("cuentas").withTitle("Cuentas del cliente")
                    ));

            return this.pagedResourcesAssembler.toModel(page);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public List<Cliente> obtenerClientes() {
        try {
            return this.servicioCliente.obtenerClientesTotales();
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {

        this.servicioCliente.insertarClienteNuevo(cliente);

        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCliente.class)
                .obtenerUnCliente(cliente.documento);

        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();

        return ResponseEntity
                .ok()
                .location(enlaceEsteDocumento)
                .build();
    }


    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerUnCliente(@PathVariable String documento) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnCliente(documento)).withSelfRel();
            final Link clientes = linkTo(methodOn(this.getClass()).obtenerClientes(1,100)).withRel(" Listar clientes: ");
            final Cliente cliente= this.servicioCliente.obtenerCliente(documento);
            return EntityModel.of(cliente).add(self,clientes);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PatchMapping("/{documento}")
    public void emparcharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.emparcharCliente(cliente);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, x.getMessage());
        }
    }
}
