package com.hackaton.controlador;

public class Rutas {
    public static final String BASE = "/api/v1";
    public static final String CLIENTES = BASE + "/clientes";
    public static final String RECLAMOS = BASE + "/reclamos";
    public static final String PRODUCTOS = BASE + "/productos";
    public static final String EXTERNAL_BASE_URL = "http://localhost:9001";
}
