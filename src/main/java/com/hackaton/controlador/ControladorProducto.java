package com.hackaton.controlador;

import com.hackaton.modelos.Producto;
import com.hackaton.servicios.ObjetoNoEncontrado;
import com.hackaton.servicios.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.PRODUCTOS)
public class ControladorProducto {
    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;
    @GetMapping
    public List<Producto> obtenerProductos() {
        try {
            return this.servicioProducto.obtenerProductos();
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity agregarProducto(@RequestBody Producto producto) {

        try {
            this.servicioProducto.insertarProductoNuevo(producto);
        } catch(ObjetoNoEncontrado x ){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{nroProducto}")
    public EntityModel<Producto> obtenerUnProducto(@PathVariable String nroProducto) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnProducto(nroProducto)).withSelfRel();
            final Link cliente = linkTo(methodOn(this.getClass()).obtenerUnProducto(nroProducto)).withRel(" El producto es: ");
            final Producto producto= this.servicioProducto.obtenerProducto(nroProducto);
            return EntityModel.of(producto).add(self,cliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PutMapping("/{nroProducto}")
    public void reemplazarUnProducto(@PathVariable("nroProducto") String nroProducto,
                                    @RequestBody Producto producto) {
        try {
            producto.codigo = nroProducto;
            this.servicioProducto.guardarProducto(producto);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PatchMapping("/{nroProducto}")
    public void emparcharUnProducto(@PathVariable("nroProducto") String nroProducto,
                                   @RequestBody Producto producto) {
        try {
            producto.codigo = nroProducto;
            this.servicioProducto.emparcharProducto(producto);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @DeleteMapping("/{nroProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnProducto(@PathVariable String nroProducto) {
        try {
            this.servicioProducto.borrarProducto(nroProducto);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, x.getMessage());
        }
    }
}
