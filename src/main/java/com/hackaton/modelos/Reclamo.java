package com.hackaton.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Reclamo {
    @Id
    public String nroReclamo;
    public String descripcion;
    public String estado;
    public String dniCliente;

    public String correoCliente;
    public String telefonoCliente;
    public String direccionCliente;
    public String codigoUbigeo;
    public String producto;

}
