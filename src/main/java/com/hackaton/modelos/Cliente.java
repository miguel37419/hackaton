package com.hackaton.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class Cliente {
    @Id
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    // Jackson - Serializacion/Deserializacion JSON
    @JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();
    @JsonIgnore
    public List<String> codigosReclamos= new ArrayList<>();

}
